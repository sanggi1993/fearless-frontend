// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload")
console.log(payloadCookie)
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
    // const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
    const cookieValue = payloadCookie.value;
    const decodedPayload = atob(cookieValue);

  // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload);

  // Print the payload
    console.log(payload);
    const permission = payload.user.perms;

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
    if (permission.includes('events.add_conference')) {
        document.getElementById('conferencePermission').classList.remove('d-none');
    if (permission.includes('events.add_location')) {
        document.getElementById('locationPermission').classList.remove('d-none');
    }
    }


  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link

}
