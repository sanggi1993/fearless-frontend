import logo from './logo.svg';
// import './App.css';
import Nav from './Nav.js'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm.js'
import ConferenceForm from './ConferenceForm.js'
import AttendConferenceForm from './AttendConferenceForm.js'
import PresentationForm from './PresentationForm.js'
import MainPage from './MainPage.js'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div>
      <Routes>
          <Route index element={<MainPage />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          {/* <Route path="/attendees/new" element={<AttendConferenceForm />} /> */}
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route path="attendees">
            <Route
              index
              element={<AttendeesList attendees={props.attendees} />}
            />
            <Route path="new" element={<AttendConferenceForm />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
